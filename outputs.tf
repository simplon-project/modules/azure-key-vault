output "azurerm_key_vault_name" {
  value = azurerm_key_vault.this.name
}
output "azurerm_key_vault_id" {
  value = azurerm_key_vault.this.id
}
output "azurerm_key_vault_uri" {
  description = "L'URI du Key Vault"
  value       = azurerm_key_vault.this.vault_uri
}
output "azurerm_key_vault_purge_protection_enabled" {
  description = "La protection contre la suppression est activée ou non"
  value       = azurerm_key_vault.this.purge_protection_enabled
}
output "keys_resource_ids" {
  description = "A map of key keys to resource ids."
  value = { for kk, kv in module.keys : kk => {
    resource_id             = kv.resource_id
    resource_versionless_id = kv.resource_versionless_id
    id                      = kv.id
    versionless_id          = kv.versionless_id
    }
  }
}
output "secrets_resource_ids" {
  description = "A map of secret keys to resource ids."
  value = { for sk, sv in module.secrets : sk => {
    resource_id             = sv.resource_id
    resource_versionless_id = sv.resource_versionless_id
    id                      = sv.id
    versionless_id          = sv.versionless_id
    }
  }
}
