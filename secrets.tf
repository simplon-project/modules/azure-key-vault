module "secrets" {
  source   = "./sub-modules/secrets"
  for_each = var.secrets

  key_vault_resource_id              = azurerm_key_vault.this.id
  name                               = each.value.name
  value                              = var.secrets_value[each.key]
  content_type                       = each.value.content_type
  expiration_date                    = each.value.expiration_date
  not_before_date                    = each.value.not_before_date
  tags                               = each.value.tags
  create_role_assignment_for_secrets = var.create_role_assignment_for_secrets
  role_assignments                   = each.value.role_assignments
}
