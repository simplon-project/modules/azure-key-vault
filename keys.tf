module "keys" {
  source   = "./sub-modules/keys"
  for_each = var.keys

  name                            = each.value.name
  key_vault_resource_id           = azurerm_key_vault.this.id
  key_type                        = each.value.key_type
  key_opts                        = each.value.key_opts
  key_size                        = each.value.key_size
  curve                           = each.value.curve
  not_before_date                 = each.value.not_before_date
  expiration_date                 = each.value.expiration_date
  tags                            = each.value.tags
  rotation_policy                 = each.value.rotation_policy
  create_role_assignment_for_keys = var.create_role_assignment_for_keys
  role_assignments                = each.value.role_assignments
}
