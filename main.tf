data "azurerm_client_config" "current" {}
locals {
  role_definition_resource_substring = "/providers/Microsoft.Authorization/roleDefinitions"
}

#tfsec:ignore:azure-keyvault-no-purge
resource "azurerm_key_vault" "this" {
  location                   = var.location
  name                       = var.name
  resource_group_name        = var.resource_group_name
  sku_name                   = var.sku_name
  tenant_id                  = data.azurerm_client_config.current.tenant_id
  enabled_for_deployment     = var.enabled_for_deployment
  soft_delete_retention_days = var.soft_delete_retention_days

  purge_protection_enabled        = var.purge_protection_enabled
  enabled_for_template_deployment = var.enabled_for_template_deployment
  public_network_access_enabled   = var.public_network_access_enabled
  tags = merge(var.tags, {
    project = var.project
    owner   = var.owner
  })


  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    certificate_permissions = var.certificate_permissions
    key_permissions         = var.key_permissions
    secret_permissions      = var.secret_permissions
    storage_permissions     = var.storage_permissions
  }
  # Seul un bloc network_acls est autorisé.
  # Le créer si la variable n'est pas nulle
  dynamic "network_acls" {
    for_each = var.network_acls != null ? { this = var.network_acls } : {}
    content {
      bypass                     = network_acls.value.bypass
      default_action             = network_acls.value.default_action
      ip_rules                   = network_acls.value.ip_rules
      virtual_network_subnet_ids = network_acls.value.virtual_network_subnet_ids
    }
  }
}

# mettre la variable `create_role_assignment_for_keys` a true pour ne pas créer les rôles
resource "azurerm_role_assignment" "this" {
  for_each = var.create_role_assignment_for_keyvault ? var.role_assignments : {}

  principal_id                           = each.value.principal_id
  scope                                  = azurerm_key_vault.this.id
  condition                              = each.value.condition
  condition_version                      = each.value.condition_version
  delegated_managed_identity_resource_id = each.value.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.skip_service_principal_aad_check
}
