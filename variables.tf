variable "location" {
  description = "la localisation du coffre de clés."
  type        = string
}
variable "name" {
  type        = string
  description = "Nom du coffre de clés."

  validation {
    condition     = can(regex("^[a-z0-9-]{3,24}$", var.name))
    error_message = "Le nom doit être composé de 3 à 24 caractères alphanumériques en minuscules et tirets."
  }
  validation {
    error_message = "Le nom ne doit pas contenir de séquences '--'."
    condition     = !can(regex("--", var.name))
  }
  validation {
    error_message = "Le nom doit commencer par une lettre."
    condition     = can(regex("^[a-zA-Z]", var.name))
  }
  validation {
    error_message = "Le nom doit se terminer par une lettre ou un chiffre."
    condition     = can(regex("[a-zA-Z0-9]$", var.name))
  }
}
variable "resource_group_name" {
  description = "Le nom du groupe de ressources."
  type        = string
}
variable "sku_name" {
  description = "Le nom du SKU."
  type        = string
  default     = "standard"
}
variable "enabled_for_deployment" {
  description = "Active le déploiement."
  type        = bool
  default     = false
}
variable "soft_delete_retention_days" {
  description = "Le nombre de jours de rétention des éléments supprimés."
  type        = number
  default     = 7
}
variable "purge_protection_enabled" {
  description = "Active la protection contre la purge."
  type        = bool
  default     = false
}
variable "enabled_for_template_deployment" {
  description = "spécifie si ARM est autorisé à récupérer des secrets."
  type        = bool
  default     = false
}
variable "public_network_access_enabled" {
  description = "Active l'accès au réseau public."
  type        = bool
  default     = true
}
variable "certificate_permissions" {
  type        = list(string)
  description = "Lise des permissions pour les certificats."
  default = [
    "Backup",
    "Create",
    "Delete",
    "DeleteIssuers",
    "Get",
    "GetIssuers",
    "Import",
    "List",
    "ListIssuers",
    "ManageContacts",
    "ManageIssuers",
    "Purge",
    "Recover",
    "Restore",
    "SetIssuers",
    "Update"
  ]
}
variable "key_permissions" {
  type        = list(string)
  description = "Liste des permissions pour les clés."
  default = [
    "Backup",
    "Create",
    "Decrypt",
    "Delete",
    "Encrypt",
    "Get",
    "Import",
    "List",
    "Purge",
    "Recover",
    "Restore",
    "Sign",
    "UnwrapKey",
    "Update",
    "Verify",
    "WrapKey",
    "Release",
    "Rotate",
    "GetRotationPolicy",
    "SetRotationPolicy"
  ]
}
variable "storage_permissions" {
  type        = list(string)
  description = "Liste des permissions pour les stockages."
  default = [
    "Backup",
    "Delete",
    "DeleteSAS",
    "Get",
    "GetSAS",
    "List",
    "ListSAS",
    "Purge",
    "Recover",
    "RegenerateKey",
    "Restore",
    "Set",
    "SetSAS",
    "Update"
  ]
}
variable "secret_permissions" {
  type        = list(string)
  description = "Liste des permissions pour les secrets"
  default     = ["List", "Set", "Delete", "Get", "Purge", "Recover", "Backup", "Restore"]
}
variable "network_acls" {
  type = object({
    bypass                     = optional(string, "None")
    default_action             = optional(string, "Deny")
    ip_rules                   = optional(list(string), [])
    virtual_network_subnet_ids = optional(list(string), [])
  })
  default     = {}
  description = <<DESCRIPTION
La configuration de l'ACL réseau pour le Key Vault.
Si non spécifié, le Key Vault sera créé avec un pare-feu qui bloque l'accès.
Spécifiez `null` pour créer le Key Vault sans pare-feu.

- `bypass` - (Facultatif) Les services Azure doivent-ils contourner l'ACL. Les valeurs possibles sont `AzureServices` et `None`. Par défaut, c'est `None`.
- `default_action` - (Facultatif) L'action par défaut lorsqu'aucune règle ne correspond. Les valeurs possibles sont `Allow` et `Deny`. Par défaut, c'est `Deny`.
- `ip_rules` - (Facultatif) Une liste de règles IP au format CIDR. Par défaut, c'est `[]`.
- `virtual_network_subnet_ids` - (Facultatif) Lors de l'utilisation avec des points de terminaison de service, une liste d'ID de sous-réseau à associer au Key Vault. Par défaut, c'est `[]`.
 DESCRIPTION
  validation {
    condition     = var.network_acls == null ? true : contains(["AzureServices", "None"], var.network_acls.bypass)
    error_message = "La valeur de bypass doit être soit `AzureServices` soit `None`."
  }
  validation {
    condition     = var.network_acls == null ? true : contains(["Allow", "Deny"], var.network_acls.default_action)
    error_message = "La valeur de default_action doit être soit `Allow` soit `Deny`."
  }
}

variable "create_role_assignment_for_keyvault" {
  description = "Indique si une affectation de rôle doit être créée pour le coffre de clé."
  type        = bool
  default     = false
}

variable "role_assignments" {
  type = map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false)
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
    principal_type                         = optional(string, null)
  }))
  default     = {}
  description = <<DESCRIPTION
A map of role assignments to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.

- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.
- `principal_id` - The ID of the principal to assign the role to.
- `description` - The description of the role assignment.
- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.
- `condition` - The condition which will be used to scope the role assignment.
- `condition_version` - The version of the condition syntax. If you are using a condition, valid values are '2.0'.

> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.
DESCRIPTION
  nullable    = false
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Un mapping de tags à assigner au coffre de clés."
}
variable "project" {
  description = "Le nom du projet."
  type        = string
  default     = null
}
variable "owner" {
  description = "Le nom du propriétaire."
  type        = string
  default     = null
}

############################################################################################################
# Path:keyvault/sub-modules/keys/variables.tf
variable "create_role_assignment_for_keys" {
  description = "Indique si une affectation de rôle doit être créée pour les clés."
  type        = bool
  default     = false
}
variable "keys" {
  type = map(object({
    name                  = string
    key_vault_resource_id = string
    key_type              = string
    key_opts              = optional(list(string), ["sign", "verify"])
    key_size              = optional(number, null)
    curve                 = optional(string, null)
    not_before_date       = optional(string, null)
    expiration_date       = optional(string, null)
    tags                  = optional(map(any), null)

    role_assignments = optional(map(object({
      role_definition_id_or_name             = string
      principal_id                           = string
      description                            = optional(string, null)
      skip_service_principal_aad_check       = optional(bool, false)
      condition                              = optional(string, null)
      condition_version                      = optional(string, null)
      delegated_managed_identity_resource_id = optional(string, null)
      principal_type                         = optional(string, null)
    })), {})

    rotation_policy = optional(object({
      automatic = optional(object({
        time_after_creation = optional(string, null)
        time_before_expiry  = optional(string, null)
      }), null)
      expire_after         = optional(string, null)
      notify_before_expiry = optional(string, null)
    }), null)
  }))
  default     = {}
  description = <<DESCRIPTION
Une carte de clés à créer sur le Key Vault. La clé de la carte est délibérément arbitraire pour éviter les problèmes où les clés de la carte peuvent être inconnues au moment du plan.

- `name` - Le nom de la clé.
- `key_type` - Le type de la clé. Les valeurs possibles sont `EC` et `RSA`.
- `key_opts` - Une liste d'options de clé. Les valeurs possibles sont `decrypt`, `encrypt`, `sign`, `unwrapKey`, `verify`, et `wrapKey`.
- `key_size` - La taille de la clé. Requis pour les clés `RSA`.
- `curve` - La courbe de la clé. Requis pour les clés `EC`. Les valeurs possibles sont `P-256`, `P-256K`, `P-384`, et `P-521`. L'API utilisera `P-256` par défaut si rien n'est spécifié.
- `not_before_date` - La date de non validité de la clé.
- `expiration_date` - La date d'expiration de la clé.
- `tags` - Une carte de tags à attribuer à la clé.
- `rotation_policy` - La politique de rotation de la clé.
  - `automatic` - La politique de rotation automatique de la clé.
    - `time_after_creation` - Le temps après la création de la clé avant qu'elle ne soit automatiquement tournée.
    - `time_before_expiry` - Le temps avant l'expiration de la clé avant qu'elle ne soit automatiquement tournée.
  - `expire_after` - Le temps après lequel la clé expire.
  - `notify_before_expiry` - Le temps avant l'expiration de la clé lorsque des emails de notification seront envoyés.

Fournissez des attributions de rôle de la même manière que pour `var.role_assignments`.
DESCRIPTION
  nullable    = false
}

#########################################################################################################
# Path:keyvault/sub-modules/secrets/variables.tf
variable "create_role_assignment_for_secrets" {
  description = "Indique si une affectation de rôle doit être créée pour les secrets."
  type        = bool
  default     = false
}

variable "secrets" {
  type = map(object({
    name            = string
    content_type    = optional(string, null)
    tags            = optional(map(any), null)
    not_before_date = optional(string, null)
    expiration_date = optional(string, null)

    role_assignments = optional(map(object({
      role_definition_id_or_name             = string
      principal_id                           = string
      description                            = optional(string, null)
      skip_service_principal_aad_check       = optional(bool, false)
      condition                              = optional(string, null)
      condition_version                      = optional(string, null)
      delegated_managed_identity_resource_id = optional(string, null)
      principal_type                         = optional(string, null)
    })), {})
  }))
  default     = {}
  description = <<DESCRIPTION
A map of secrets to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.

- `name` - The name of the secret.
- `content_type` - The content type of the secret.
- `tags` - A mapping of tags to assign to the secret.
- `not_before_date` - The not before date of the secret.
- `expiration_date` - The expiration date of the secret.

Supply role assignments in the same way as for `var.role_assignments`.

> Note: the `value` of the secret is supplied via the `var.secrets_value` variable. Make sure to use the same map key.
DESCRIPTION
  nullable    = false
}

variable "secrets_value" {
  type        = map(string)
  default     = null
  description = <<DESCRIPTION
A map of secret keys to values.
The map key is the supplied input to `var.secrets`.
The map value is the secret value.

This is a separate variable to `var.secrets` because it is sensitive and therefore cannot be used in a `for_each` loop.
DESCRIPTION
  sensitive   = true
}
