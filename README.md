<!-- BEGIN_TF_DOCS -->


<!-- markdownlint-disable MD033 -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.9.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 3.110.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 3.110.0 |

## Resources

| Name | Type |
|------|------|
| [azurerm_key_vault.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault) | resource |
| [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment) | resource |
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/client_config) | data source |

<!-- markdownlint-disable MD013 -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_location"></a> [location](#input\_location) | la localisation du coffre de clés. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Nom du coffre de clés. | `string` | n/a | yes |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Le nom du groupe de ressources. | `string` | n/a | yes |
| <a name="input_certificate_permissions"></a> [certificate\_permissions](#input\_certificate\_permissions) | Lise des permissions pour les certificats. | `list(string)` | <pre>[<br>  "Backup",<br>  "Create",<br>  "Delete",<br>  "DeleteIssuers",<br>  "Get",<br>  "GetIssuers",<br>  "Import",<br>  "List",<br>  "ListIssuers",<br>  "ManageContacts",<br>  "ManageIssuers",<br>  "Purge",<br>  "Recover",<br>  "Restore",<br>  "SetIssuers",<br>  "Update"<br>]</pre> | no |
| <a name="input_create_role_assignment_for_keys"></a> [create\_role\_assignment\_for\_keys](#input\_create\_role\_assignment\_for\_keys) | Indique si une affectation de rôle doit être créée pour les clés. | `bool` | `false` | no |
| <a name="input_create_role_assignment_for_keyvault"></a> [create\_role\_assignment\_for\_keyvault](#input\_create\_role\_assignment\_for\_keyvault) | Indique si une affectation de rôle doit être créée pour le coffre de clé. | `bool` | `false` | no |
| <a name="input_create_role_assignment_for_secrets"></a> [create\_role\_assignment\_for\_secrets](#input\_create\_role\_assignment\_for\_secrets) | Indique si une affectation de rôle doit être créée pour les secrets. | `bool` | `false` | no |
| <a name="input_enabled_for_deployment"></a> [enabled\_for\_deployment](#input\_enabled\_for\_deployment) | Active le déploiement. | `bool` | `false` | no |
| <a name="input_enabled_for_template_deployment"></a> [enabled\_for\_template\_deployment](#input\_enabled\_for\_template\_deployment) | spécifie si ARM est autorisé à récupérer des secrets. | `bool` | `false` | no |
| <a name="input_key_permissions"></a> [key\_permissions](#input\_key\_permissions) | Liste des permissions pour les clés. | `list(string)` | <pre>[<br>  "Backup",<br>  "Create",<br>  "Decrypt",<br>  "Delete",<br>  "Encrypt",<br>  "Get",<br>  "Import",<br>  "List",<br>  "Purge",<br>  "Recover",<br>  "Restore",<br>  "Sign",<br>  "UnwrapKey",<br>  "Update",<br>  "Verify",<br>  "WrapKey",<br>  "Release",<br>  "Rotate",<br>  "GetRotationPolicy",<br>  "SetRotationPolicy"<br>]</pre> | no |
| <a name="input_keys"></a> [keys](#input\_keys) | Une carte de clés à créer sur le Key Vault. La clé de la carte est délibérément arbitraire pour éviter les problèmes où les clés de la carte peuvent être inconnues au moment du plan.<br><br>- `name` - Le nom de la clé.<br>- `key_type` - Le type de la clé. Les valeurs possibles sont `EC` et `RSA`.<br>- `key_opts` - Une liste d'options de clé. Les valeurs possibles sont `decrypt`, `encrypt`, `sign`, `unwrapKey`, `verify`, et `wrapKey`.<br>- `key_size` - La taille de la clé. Requis pour les clés `RSA`.<br>- `curve` - La courbe de la clé. Requis pour les clés `EC`. Les valeurs possibles sont `P-256`, `P-256K`, `P-384`, et `P-521`. L'API utilisera `P-256` par défaut si rien n'est spécifié.<br>- `not_before_date` - La date de non validité de la clé.<br>- `expiration_date` - La date d'expiration de la clé.<br>- `tags` - Une carte de tags à attribuer à la clé.<br>- `rotation_policy` - La politique de rotation de la clé.<br>  - `automatic` - La politique de rotation automatique de la clé.<br>    - `time_after_creation` - Le temps après la création de la clé avant qu'elle ne soit automatiquement tournée.<br>    - `time_before_expiry` - Le temps avant l'expiration de la clé avant qu'elle ne soit automatiquement tournée.<br>  - `expire_after` - Le temps après lequel la clé expire.<br>  - `notify_before_expiry` - Le temps avant l'expiration de la clé lorsque des emails de notification seront envoyés.<br><br>Fournissez des attributions de rôle de la même manière que pour `var.role_assignments`. | <pre>map(object({<br>    name                  = string<br>    key_vault_resource_id = string<br>    key_type              = string<br>    key_opts              = optional(list(string), ["sign", "verify"])<br>    key_size              = optional(number, null)<br>    curve                 = optional(string, null)<br>    not_before_date       = optional(string, null)<br>    expiration_date       = optional(string, null)<br>    tags                  = optional(map(any), null)<br><br>    role_assignments = optional(map(object({<br>      role_definition_id_or_name             = string<br>      principal_id                           = string<br>      description                            = optional(string, null)<br>      skip_service_principal_aad_check       = optional(bool, false)<br>      condition                              = optional(string, null)<br>      condition_version                      = optional(string, null)<br>      delegated_managed_identity_resource_id = optional(string, null)<br>      principal_type                         = optional(string, null)<br>    })), {})<br><br>    rotation_policy = optional(object({<br>      automatic = optional(object({<br>        time_after_creation = optional(string, null)<br>        time_before_expiry  = optional(string, null)<br>      }), null)<br>      expire_after         = optional(string, null)<br>      notify_before_expiry = optional(string, null)<br>    }), null)<br>  }))</pre> | `{}` | no |
| <a name="input_network_acls"></a> [network\_acls](#input\_network\_acls) | La configuration de l'ACL réseau pour le Key Vault.<br>Si non spécifié, le Key Vault sera créé avec un pare-feu qui bloque l'accès.<br>Spécifiez `null` pour créer le Key Vault sans pare-feu.<br><br>- `bypass` - (Facultatif) Les services Azure doivent-ils contourner l'ACL. Les valeurs possibles sont `AzureServices` et `None`. Par défaut, c'est `None`.<br>- `default_action` - (Facultatif) L'action par défaut lorsqu'aucune règle ne correspond. Les valeurs possibles sont `Allow` et `Deny`. Par défaut, c'est `Deny`.<br>- `ip_rules` - (Facultatif) Une liste de règles IP au format CIDR. Par défaut, c'est `[]`.<br>- `virtual_network_subnet_ids` - (Facultatif) Lors de l'utilisation avec des points de terminaison de service, une liste d'ID de sous-réseau à associer au Key Vault. Par défaut, c'est `[]`. | <pre>object({<br>    bypass                     = optional(string, "None")<br>    default_action             = optional(string, "Deny")<br>    ip_rules                   = optional(list(string), [])<br>    virtual_network_subnet_ids = optional(list(string), [])<br>  })</pre> | `{}` | no |
| <a name="input_owner"></a> [owner](#input\_owner) | Le nom du propriétaire. | `string` | `null` | no |
| <a name="input_project"></a> [project](#input\_project) | Le nom du projet. | `string` | `null` | no |
| <a name="input_public_network_access_enabled"></a> [public\_network\_access\_enabled](#input\_public\_network\_access\_enabled) | Active l'accès au réseau public. | `bool` | `true` | no |
| <a name="input_purge_protection_enabled"></a> [purge\_protection\_enabled](#input\_purge\_protection\_enabled) | Active la protection contre la purge. | `bool` | `false` | no |
| <a name="input_role_assignments"></a> [role\_assignments](#input\_role\_assignments) | A map of role assignments to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.<br><br>- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.<br>- `principal_id` - The ID of the principal to assign the role to.<br>- `description` - The description of the role assignment.<br>- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.<br>- `condition` - The condition which will be used to scope the role assignment.<br>- `condition_version` - The version of the condition syntax. If you are using a condition, valid values are '2.0'.<br><br>> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal. | <pre>map(object({<br>    role_definition_id_or_name             = string<br>    principal_id                           = string<br>    description                            = optional(string, null)<br>    skip_service_principal_aad_check       = optional(bool, false)<br>    condition                              = optional(string, null)<br>    condition_version                      = optional(string, null)<br>    delegated_managed_identity_resource_id = optional(string, null)<br>    principal_type                         = optional(string, null)<br>  }))</pre> | `{}` | no |
| <a name="input_secret_permissions"></a> [secret\_permissions](#input\_secret\_permissions) | Liste des permissions pour les secrets | `list(string)` | <pre>[<br>  "List",<br>  "Set",<br>  "Delete",<br>  "Get",<br>  "Purge",<br>  "Recover",<br>  "Backup",<br>  "Restore"<br>]</pre> | no |
| <a name="input_secrets"></a> [secrets](#input\_secrets) | A map of secrets to create on the Key Vault. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.<br><br>- `name` - The name of the secret.<br>- `content_type` - The content type of the secret.<br>- `tags` - A mapping of tags to assign to the secret.<br>- `not_before_date` - The not before date of the secret.<br>- `expiration_date` - The expiration date of the secret.<br><br>Supply role assignments in the same way as for `var.role_assignments`.<br><br>> Note: the `value` of the secret is supplied via the `var.secrets_value` variable. Make sure to use the same map key. | <pre>map(object({<br>    name            = string<br>    content_type    = optional(string, null)<br>    tags            = optional(map(any), null)<br>    not_before_date = optional(string, null)<br>    expiration_date = optional(string, null)<br><br>    role_assignments = optional(map(object({<br>      role_definition_id_or_name             = string<br>      principal_id                           = string<br>      description                            = optional(string, null)<br>      skip_service_principal_aad_check       = optional(bool, false)<br>      condition                              = optional(string, null)<br>      condition_version                      = optional(string, null)<br>      delegated_managed_identity_resource_id = optional(string, null)<br>      principal_type                         = optional(string, null)<br>    })), {})<br>  }))</pre> | `{}` | no |
| <a name="input_secrets_value"></a> [secrets\_value](#input\_secrets\_value) | A map of secret keys to values.<br>The map key is the supplied input to `var.secrets`.<br>The map value is the secret value.<br><br>This is a separate variable to `var.secrets` because it is sensitive and therefore cannot be used in a `for_each` loop. | `map(string)` | `null` | no |
| <a name="input_sku_name"></a> [sku\_name](#input\_sku\_name) | Le nom du SKU. | `string` | `"standard"` | no |
| <a name="input_soft_delete_retention_days"></a> [soft\_delete\_retention\_days](#input\_soft\_delete\_retention\_days) | Le nombre de jours de rétention des éléments supprimés. | `number` | `7` | no |
| <a name="input_storage_permissions"></a> [storage\_permissions](#input\_storage\_permissions) | Liste des permissions pour les stockages. | `list(string)` | <pre>[<br>  "Backup",<br>  "Delete",<br>  "DeleteSAS",<br>  "Get",<br>  "GetSAS",<br>  "List",<br>  "ListSAS",<br>  "Purge",<br>  "Recover",<br>  "RegenerateKey",<br>  "Restore",<br>  "Set",<br>  "SetSAS",<br>  "Update"<br>]</pre> | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Un mapping de tags à assigner au coffre de clés. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_azurerm_key_vault_id"></a> [azurerm\_key\_vault\_id](#output\_azurerm\_key\_vault\_id) | n/a |
| <a name="output_azurerm_key_vault_name"></a> [azurerm\_key\_vault\_name](#output\_azurerm\_key\_vault\_name) | n/a |
| <a name="output_azurerm_key_vault_purge_protection_enabled"></a> [azurerm\_key\_vault\_purge\_protection\_enabled](#output\_azurerm\_key\_vault\_purge\_protection\_enabled) | La protection contre la suppression est activée ou non |
| <a name="output_azurerm_key_vault_uri"></a> [azurerm\_key\_vault\_uri](#output\_azurerm\_key\_vault\_uri) | L'URI du Key Vault |
| <a name="output_keys_resource_ids"></a> [keys\_resource\_ids](#output\_keys\_resource\_ids) | A map of key keys to resource ids. |
| <a name="output_secrets_resource_ids"></a> [secrets\_resource\_ids](#output\_secrets\_resource\_ids) | A map of secret keys to resource ids. |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_keys"></a> [keys](#module\_keys) | ./sub-modules/keys | n/a |
| <a name="module_secrets"></a> [secrets](#module\_secrets) | ./sub-modules/secrets | n/a |

<!-- END_TF_DOCS -->
