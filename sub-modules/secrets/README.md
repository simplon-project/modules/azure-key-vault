<!-- BEGIN_TF_DOCS -->
 # Module: Keyvault/sub-modules/secret

Ce sous-module permet de crée les secrets dans un Azure Key Vault.
Il offre des fonctionnalités pour créer, récupérer, mettre à jour et supprimer des secrets dans un coffre-fort Key Vault. Les secrets peuvent être utilisés pour stocker des informations sensibles telles que des clés d'API, des chaînes de connexion de base de données, des certificats.

Utilisez ce module pour sécuriser vos secrets et garantir leur confidentialité dans votre application ou votre infrastructure Azure.

```hcl
resource "azurerm_key_vault_secret" "this" {
  key_vault_id    = var.key_vault_resource_id
  name            = var.name
  value           = var.value
  content_type    = var.content_type
  expiration_date = var.expiration_date
  not_before_date = var.not_before_date
  tags            = var.tags
}

resource "azurerm_role_assignment" "this" {
  for_each = var.role_assignments

  principal_id                           = each.value.role_assignment.principal_id
  scope                                  = azurerm_key_vault_secret.this
  condition                              = each.value.role_assignment.condition
  condition_version                      = each.value.role_assignment.condition_version
  delegated_managed_identity_resource_id = each.value.role_assignment.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_assignment.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_assignment.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.role_assignment.skip_service_principal_aad_check
}
```

<!-- markdownlint-disable MD033 -->
## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (>= 1.0.0)

- <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) (>= 3.110.0)

## Providers

The following providers are used by this module:

- <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) (>= 3.110.0)

## Resources

The following resources are used by this module:

- [azurerm_key_vault_secret.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret) (resource)
- [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment) (resource)

<!-- markdownlint-disable MD013 -->
## Required Inputs

The following input variables are required:

### <a name="input_key_vault_resource_id"></a> [key\_vault\_resource\_id](#input\_key\_vault\_resource\_id)

Description: The ID of the Key Vault where the secret should be created.

Type: `string`

### <a name="input_name"></a> [name](#input\_name)

Description: The name of the secret.

Type: `string`

### <a name="input_value"></a> [value](#input\_value)

Description: The value for the secret.

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_content_type"></a> [content\_type](#input\_content\_type)

Description: The content type of the secret.

Type: `string`

Default: `null`

### <a name="input_expiration_date"></a> [expiration\_date](#input\_expiration\_date)

Description: The expiration date of the secret as a UTC datetime (Y-m-d'T'H:M:S'Z').

Type: `string`

Default: `null`

### <a name="input_not_before_date"></a> [not\_before\_date](#input\_not\_before\_date)

Description: Secret not usable before as a UTC datetime (Y-m-d'T'H:M:S'Z').

Type: `string`

Default: `null`

### <a name="input_role_assignments"></a> [role\_assignments](#input\_role\_assignments)

Description: A map of role assignments to create on the secret. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.

- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.
- `principal_id` - The ID of the principal to assign the role to.
- `description` - The description of the role assignment.
- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.
- `condition` - The condition which will be used to scope the role assignment.
- `condition_version` - The version of the condition syntax. If you are using a condition, valid values are '2.0'.

> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal.

Type:

```hcl
map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false)
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
    principal_type                         = optional(string, null)
  }))
```

Default: `{}`

### <a name="input_tags"></a> [tags](#input\_tags)

Description: The tags to assign to the secret.

Type: `map(string)`

Default: `null`

## Outputs

The following outputs are exported:

### <a name="output_id"></a> [id](#output\_id)

Description: L'ID de la ressource Azure du secret.

### <a name="output_resource_id"></a> [resource\_id](#output\_resource\_id)

Description: L'ID de la ressource Azure du secret.

### <a name="output_resource_versionless_id"></a> [resource\_versionless\_id](#output\_resource\_versionless\_id)

Description: L'ID de ressource Azure sans version du secret.

### <a name="output_versionless_id"></a> [versionless\_id](#output\_versionless\_id)

Description: L'ID sans version du secret.

## Modules

No modules.

## Documentation

[**keyvault\_secret**](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)
<!-- END_TF_DOCS -->
