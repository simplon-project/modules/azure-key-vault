<!-- BEGIN_TF_DOCS -->
 # Module: Keyvault/sub-modules/secret

Ce sous-module permet de crée les secrets dans un Azure Key Vault.
Il offre des fonctionnalités pour créer, récupérer, mettre à jour et supprimer des secrets dans un coffre-fort Key Vault. Les secrets peuvent être utilisés pour stocker des informations sensibles telles que des clés d'API, des chaînes de connexion de base de données, des certificats.

Utilisez ce module pour sécuriser vos secrets et garantir leur confidentialité dans votre application ou votre infrastructure Azure.

```hcl
locals {
  role_definition_resource_substring = "/providers/Microsoft.Authorization/roleDefinitions"
}

resource "azurerm_key_vault_secret" "this" {
  key_vault_id    = var.key_vault_resource_id
  name            = var.name
  value           = var.value
  content_type    = var.content_type
  expiration_date = var.expiration_date
  not_before_date = var.not_before_date
  tags            = var.tags
}

resource "azurerm_role_assignment" "this" {
  for_each = var.create_role_assignment_for_secrets ? var.role_assignments : {}

  principal_id                           = each.value.role_assignment.principal_id
  scope                                  = azurerm_key_vault_secret.this
  condition                              = each.value.role_assignment.condition
  condition_version                      = each.value.role_assignment.condition_version
  delegated_managed_identity_resource_id = each.value.role_assignment.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_assignment.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_assignment.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.role_assignment.skip_service_principal_aad_check
}
```

<!-- markdownlint-disable MD033 -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.9.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 3.110.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 3.110.0 |

## Resources

| Name | Type |
|------|------|
| [azurerm_key_vault_secret.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret) | resource |
| [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment) | resource |

<!-- markdownlint-disable MD013 -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_key_vault_resource_id"></a> [key\_vault\_resource\_id](#input\_key\_vault\_resource\_id) | The ID of the Key Vault where the secret should be created. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | The name of the secret. | `string` | n/a | yes |
| <a name="input_value"></a> [value](#input\_value) | The value for the secret. | `string` | n/a | yes |
| <a name="input_content_type"></a> [content\_type](#input\_content\_type) | The content type of the secret. | `string` | `null` | no |
| <a name="input_create_role_assignment_for_secrets"></a> [create\_role\_assignment\_for\_secrets](#input\_create\_role\_assignment\_for\_secrets) | Indique si une affectation de rôle doit être créée pour les secrets. | `bool` | `false` | no |
| <a name="input_expiration_date"></a> [expiration\_date](#input\_expiration\_date) | The expiration date of the secret as a UTC datetime (Y-m-d'T'H:M:S'Z'). | `string` | `null` | no |
| <a name="input_not_before_date"></a> [not\_before\_date](#input\_not\_before\_date) | Secret not usable before as a UTC datetime (Y-m-d'T'H:M:S'Z'). | `string` | `null` | no |
| <a name="input_role_assignments"></a> [role\_assignments](#input\_role\_assignments) | A map of role assignments to create on the secret. The map key is deliberately arbitrary to avoid issues where map keys maybe unknown at plan time.<br><br>- `role_definition_id_or_name` - The ID or name of the role definition to assign to the principal.<br>- `principal_id` - The ID of the principal to assign the role to.<br>- `description` - The description of the role assignment.<br>- `skip_service_principal_aad_check` - If set to true, skips the Azure Active Directory check for the service principal in the tenant. Defaults to false.<br>- `condition` - The condition which will be used to scope the role assignment.<br>- `condition_version` - The version of the condition syntax. If you are using a condition, valid values are '2.0'.<br><br>> Note: only set `skip_service_principal_aad_check` to true if you are assigning a role to a service principal. | <pre>map(object({<br>    role_definition_id_or_name             = string<br>    principal_id                           = string<br>    description                            = optional(string, null)<br>    skip_service_principal_aad_check       = optional(bool, false)<br>    condition                              = optional(string, null)<br>    condition_version                      = optional(string, null)<br>    delegated_managed_identity_resource_id = optional(string, null)<br>    principal_type                         = optional(string, null)<br>  }))</pre> | `{}` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | The tags to assign to the secret. | `map(string)` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | L'ID de la ressource Azure du secret. |
| <a name="output_resource_id"></a> [resource\_id](#output\_resource\_id) | L'ID de la ressource Azure du secret. |
| <a name="output_resource_versionless_id"></a> [resource\_versionless\_id](#output\_resource\_versionless\_id) | L'ID de ressource Azure sans version du secret. |
| <a name="output_versionless_id"></a> [versionless\_id](#output\_versionless\_id) | L'ID sans version du secret. |

## Modules

No modules.

## Documentation

[**keyvault\_secret**](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)
<!-- END_TF_DOCS -->
