output "id" {
  description = "L'ID de la ressource Azure du secret."
  value       = azurerm_key_vault_secret.this.id
}

output "resource_id" {
  description = "L'ID de la ressource Azure du secret."
  value       = azurerm_key_vault_secret.this.resource_id
}

output "resource_versionless_id" {
  description = "L'ID de ressource Azure sans version du secret."
  value       = azurerm_key_vault_secret.this.resource_versionless_id
}

output "versionless_id" {
  description = "L'ID sans version du secret."
  value       = azurerm_key_vault_secret.this.versionless_id
}
