variable "cert_name" {
  description = "Le nom du certificat à importer."
  type        = string
}

variable "key_vault_id" {
  description = "L'ID du Key Vault où le certificat doit être importé."
  type        = string
}

variable "cert_file_path" {
  description = "Le chemin d'accès au fichier de certificat à importer."
  type        = string
}

variable "cert_password" {
  description = "Le mot de passe du certificat à importer."
  type        = string
  default     = ""
}
