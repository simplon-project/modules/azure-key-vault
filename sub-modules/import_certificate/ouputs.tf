output "imported_certificate_id" {
  description = "L'ID du certificat importé."
  value       = azurerm_key_vault_certificate.import.id
}
