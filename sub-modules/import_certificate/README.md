<!-- BEGIN_TF_DOCS -->
 # Module: Keyvault/sub-modules/secret

Ce sous-module permet de crée les secrets dans un Azure Key Vault.
Il offre des fonctionnalités pour créer, récupérer, mettre à jour et supprimer des secrets dans un coffre-fort Key Vault. Les secrets peuvent être utilisés pour stocker des informations sensibles telles que des clés d'API, des chaînes de connexion de base de données, des certificats.

Utilisez ce module pour sécuriser vos secrets et garantir leur confidentialité dans votre application ou votre infrastructure Azure.

```hcl

# Pour importer un certificat dans un coffre de clés Azure
resource "azurerm_key_vault_certificate" "import" {
  name         = var.cert_name
  key_vault_id = var.key_vault_id

  certificate {
    contents = filebase64(var.cert_file_path)
    password = var.cert_password
  }
}

/* vous devez fournir les valeurs pour `cert_name`, `key_vault_id`, `cert_file_path` et `cert_password `
lorsque vous utilisez ce module. Le chemin du fichier de certificat et le mot de passe du certificat sont utilisés
pour lire le certificat à partir du fichier et l’importer dans Azure Key Vault */
```

<!-- markdownlint-disable MD033 -->
## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (>= 1.0.0)

- <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) (>= 3.110.0)

## Providers

The following providers are used by this module:

- <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) (>= 3.110.0)

## Resources

The following resources are used by this module:

- [azurerm_key_vault_certificate.import](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_certificate) (resource)

<!-- markdownlint-disable MD013 -->
## Required Inputs

The following input variables are required:

### <a name="input_cert_file_path"></a> [cert\_file\_path](#input\_cert\_file\_path)

Description: Le chemin d'accès au fichier de certificat à importer.

Type: `string`

### <a name="input_cert_name"></a> [cert\_name](#input\_cert\_name)

Description: Le nom du certificat à importer.

Type: `string`

### <a name="input_key_vault_id"></a> [key\_vault\_id](#input\_key\_vault\_id)

Description: L'ID du Key Vault où le certificat doit être importé.

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_cert_password"></a> [cert\_password](#input\_cert\_password)

Description: Le mot de passe du certificat à importer.

Type: `string`

Default: `""`

## Outputs

The following outputs are exported:

### <a name="output_imported_certificate_id"></a> [imported\_certificate\_id](#output\_imported\_certificate\_id)

Description: L'ID du certificat importé.

## Modules

No modules.

## Documentation

[**keyvault\_secret**](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)
<!-- END_TF_DOCS -->
