
# Pour importer un certificat dans un coffre de clés Azure
resource "azurerm_key_vault_certificate" "import" {
  name         = var.cert_name
  key_vault_id = var.key_vault_id

  certificate {
    contents = filebase64(var.cert_file_path)
    password = var.cert_password
  }
}

/* vous devez fournir les valeurs pour `cert_name`, `key_vault_id`, `cert_file_path` et `cert_password `
lorsque vous utilisez ce module. Le chemin du fichier de certificat et le mot de passe du certificat sont utilisés
pour lire le certificat à partir du fichier et l’importer dans Azure Key Vault */
