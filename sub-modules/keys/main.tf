locals {
  role_definition_resource_substring = "/providers/Microsoft.Authorization/roleDefinitions"
}

resource "azurerm_key_vault_key" "this" {
  name            = var.name
  key_vault_id    = var.key_vault_resource_id
  key_type        = var.key_type
  key_size        = var.key_size
  key_opts        = var.key_opts
  curve           = var.curve
  expiration_date = timeadd(timestamp(), var.expiration_date)
  not_before_date = var.not_before_date
  tags            = var.tags
  dynamic "rotation_policy" {
    for_each = var.rotation_policy != null ? [var.rotation_policy] : []
    content {
      expire_after         = rotation_policy.value.expire_after
      notify_before_expiry = rotation_policy.value.notify_before_expiry

      automatic {
        time_before_expiry = rotation_policy.value.automatic.time_before_expiry
      }
    }
  }
}

resource "azurerm_role_assignment" "this" {
  for_each = var.create_role_assignment_for_keys ? var.role_assignments : {}

  principal_id                           = each.value.role_assignment.principal_id
  scope                                  = azurerm_key_vault_key.this
  condition                              = each.value.role_assignment.condition
  condition_version                      = each.value.role_assignment.condition_version
  delegated_managed_identity_resource_id = each.value.role_assignment.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_assignment.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_assignment.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.role_assignment.skip_service_principal_aad_check
}
