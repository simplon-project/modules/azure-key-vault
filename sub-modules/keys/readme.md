<!-- BEGIN_TF_DOCS -->
 # Module: Keyvault/sub-modules/keys

Ce sous-module permet de crée les clés dans un Azure Key Vault.
Il offre des fonctionnalités pour créer, récupérer, mettre à jour et supprimer des clés dans un coffre-fort.

```hcl
locals {
  role_definition_resource_substring = "/providers/Microsoft.Authorization/roleDefinitions"
}

resource "azurerm_key_vault_key" "this" {
  name            = var.name
  key_vault_id    = var.key_vault_resource_id
  key_type        = var.key_type
  key_size        = var.key_size
  key_opts        = var.key_opts
  curve           = var.curve
  expiration_date = timeadd(timestamp(), var.expiration_date)
  not_before_date = var.not_before_date
  tags            = var.tags
  dynamic "rotation_policy" {
    for_each = var.rotation_policy != null ? [var.rotation_policy] : []
    content {
      expire_after         = rotation_policy.value.expire_after
      notify_before_expiry = rotation_policy.value.notify_before_expiry

      automatic {
        time_before_expiry = rotation_policy.value.automatic.time_before_expiry
      }
    }
  }
}

resource "azurerm_role_assignment" "this" {
  for_each = var.create_role_assignment_for_keys ? var.role_assignments : {}

  principal_id                           = each.value.role_assignment.principal_id
  scope                                  = azurerm_key_vault_key.this
  condition                              = each.value.role_assignment.condition
  condition_version                      = each.value.role_assignment.condition_version
  delegated_managed_identity_resource_id = each.value.role_assignment.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_assignment.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_assignment.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.role_assignment.skip_service_principal_aad_check
}
```

<!-- markdownlint-disable MD033 -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.9.0 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | >= 3.110.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | >= 3.110.0 |

## Resources

| Name | Type |
|------|------|
| [azurerm_key_vault_key.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_key) | resource |
| [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment) | resource |

<!-- markdownlint-disable MD013 -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_key_vault_resource_id"></a> [key\_vault\_resource\_id](#input\_key\_vault\_resource\_id) | The ID of the Key Vault where the key should be created. | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Nom de la clé. | `string` | n/a | yes |
| <a name="input_create_role_assignment_for_keys"></a> [create\_role\_assignment\_for\_keys](#input\_create\_role\_assignment\_for\_keys) | Indique si une affectation de rôle doit être créée pour les clés. | `bool` | `false` | no |
| <a name="input_curve"></a> [curve](#input\_curve) | la courbe elliptique de la clé. Utilisé pour les clés de type EC et EC-HSM. Les valeurs valides sont P-256, P-384, P-521, P-256K, P-384K, P-521K. | `string` | `"P-256"` | no |
| <a name="input_expiration_date"></a> [expiration\_date](#input\_expiration\_date) | La date d'expiration de la clé au format UTC (Y-m-d'T'H:M:S'Z'),par défaut, la clé expire dans un an. | `string` | `"8760"` | no |
| <a name="input_key_opts"></a> [key\_opts](#input\_key\_opts) | Les options de la clé. | `list(string)` | <pre>[<br>  "decrypt",<br>  "encrypt",<br>  "sign",<br>  "unwrapKey",<br>  "verify",<br>  "wrapKey"<br>]</pre> | no |
| <a name="input_key_size"></a> [key\_size](#input\_key\_size) | La taille de la clé. | `number` | `2048` | no |
| <a name="input_key_type"></a> [key\_type](#input\_key\_type) | Le type de la clé. RSA, EC, EC-HSM, RSA-HSM3 | `string` | `"RSA"` | no |
| <a name="input_not_before_date"></a> [not\_before\_date](#input\_not\_before\_date) | La date à partir de laquelle la clé peut être utilisée au format UTC (Y-m-d'T'H:M:S'Z').par défaut, la clé peut être utilisée immédiatement. | `string` | `null` | no |
| <a name="input_role_assignments"></a> [role\_assignments](#input\_role\_assignments) | la map des affectations de rôles à créer sur la clé. La clé de la map est délibérément optionnel pour éviter les problèmes où les clés de la map peuvent être inconnues au moment du plan.<br><br>- `role_definition_id_or_name` - L'ID ou le nom de la définition de rôle à attribuer au principal. "Reader" et "Contributor" sont des exemples de noms de définition de rôle.<br>- `principal_id` - L'ID du principal à qui attribuer le rôle. Cela peut être l'ID d'un utilisateur, d'un groupe ou d'un service principal.<br>- `description` - La description de l'affectation de rôle.<br>- `skip_service_principal_aad_check` - Si défini sur true, ignore la vérification Azure Active Directory pour le service principal dans le locataire. Par défaut à false.<br>- `condition` - La condition qui sera utilisée pour délimiter l'affectation de rôle. La syntaxe de la condition est basée sur Azure Resource Manager. Pour plus d'informations, consultez https://learn.microsoft.com/en-us/azure/role-based-access-control/role-assignments.<br>- `condition_version` - La version de la syntaxe de condition. Si vous utilisez une condition, les valeurs valides sont '2.0'.<br><br>> Remarque : ne définissez `skip_service_principal_aad_check` sur vrai que si vous attribuez un rôle à un service principal. | <pre>map(object({<br>    role_definition_id_or_name             = string<br>    principal_id                           = string<br>    description                            = optional(string, null)<br>    skip_service_principal_aad_check       = optional(bool, false)<br>    condition                              = optional(string, null)<br>    condition_version                      = optional(string, null)<br>    delegated_managed_identity_resource_id = optional(string, null)<br>    principal_type                         = optional(string, null)<br>  }))</pre> | `{}` | no |
| <a name="input_rotation_policy"></a> [rotation\_policy](#input\_rotation\_policy) | La politique de rotation de la clé :<br><br>- `automatic` - La politique de rotation automatique de la clé.<br>  - `time_after_creation` - Le temps après la création de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.<br>  - `time_before_expiry` - Le temps avant l'expiration de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.<br>- `expire_after` - Le temps après lequel la clé expire.<br>- `notify_before_expiry` - Le temps avant l'expiration de la clé lorsque des emails de notification seront envoyés, exprimé en durée ISO 8601. | <pre>object({<br>    automatic = optional(object({<br>      time_after_creation = optional(string, null)<br>      time_before_expiry  = optional(string, null)<br>    }), null)<br>    expire_after         = optional(string, null)<br>    notify_before_expiry = optional(string, null)<br>  })</pre> | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Un mapping de tags à assigner à la clé. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_key_id"></a> [key\_id](#output\_key\_id) | La clé générée par le Key Vault |
| <a name="output_resource_id"></a> [resource\_id](#output\_resource\_id) | l'id du secret généré par le Key Vault |
| <a name="output_resource_versionless_id"></a> [resource\_versionless\_id](#output\_resource\_versionless\_id) | L'ID de ressource Azure sans version du secret. |
| <a name="output_versionless_id"></a> [versionless\_id](#output\_versionless\_id) | L'ID de base de la clé du Key Vault |

## Modules

No modules.

## Documentation

[**keyvault\_secret**](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)
<!-- END_TF_DOCS -->
