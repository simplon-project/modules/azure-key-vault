output "key_id" {
  description = "La clé générée par le Key Vault"
  value       = azurerm_key_vault_key.this.id
}
output "resource_id" {
  description = "l'id du secret généré par le Key Vault"
  value       = azurerm_key_vault_key.this.resource_id
}

output "resource_versionless_id" {
  description = "L'ID de ressource Azure sans version du secret."
  value       = azurerm_key_vault_key.this.resource_versionless_id
}

output "versionless_id" {
  description = "L'ID de base de la clé du Key Vault"
  value       = azurerm_key_vault_key.this.versionless_id
}
