
variable "name" {
  type        = string
  description = "Nom de la clé."
  nullable    = false
}
variable "key_vault_resource_id" {
  type        = string
  description = "The ID of the Key Vault where the key should be created."
  nullable    = false

  validation {
    error_message = "Veuillez fournir un ID de ressource valide pour le coffre de clés"
    condition     = can(regex("\\/subscriptions\\/[a-f\\d]{4}(?:[a-f\\d]{4}-){4}[a-f\\d]{12}\\/resourceGroups\\/[^\\/]+\\/providers\\/Microsoft.KeyVault\\/vaults\\/[^\\/]+$", var.key_vault_resource_id))
  }
}
variable "key_type" {
  description = "Le type de la clé. RSA, EC, EC-HSM, RSA-HSM3"
  type        = string
  default     = "RSA"

  validation {
    condition     = contains(["RSA", "EC", "EC-HSM", "RSA-HSM3"], var.key_type)
    error_message = "La valeur de key_type doit être RSA, EC, EC-HSM, ou RSA-HSM3."
  }
}
variable "key_size" {
  description = "La taille de la clé."
  type        = number
  default     = 2048

  validation {
    condition     = var.key_size > 0 && var.key_size <= 4096
    error_message = "La valeur de key_size doit être un nombre entre 1 et 4096."
  }
}
variable "key_opts" {
  description = "Les options de la clé."
  type        = list(string)
  default     = ["decrypt", "encrypt", "sign", "unwrapKey", "verify", "wrapKey"]
}
variable "curve" {
  type        = string
  default     = "P-256"
  description = " la courbe elliptique de la clé. Utilisé pour les clés de type EC et EC-HSM. Les valeurs valides sont P-256, P-384, P-521, P-256K, P-384K, P-521K."
}
variable "expiration_date" {
  type        = string
  default     = "8760"
  description = "La date d'expiration de la clé au format UTC (Y-m-d'T'H:M:S'Z'),par défaut, la clé expire dans un an."

  validation {
    error_message = "Veuillez fournir une date d'expiration valide pour la clé."
    condition     = var.expiration_date == null ? true : can(regex("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$", var.expiration_date))
  }
}
variable "not_before_date" {
  type        = string
  default     = null
  description = "La date à partir de laquelle la clé peut être utilisée au format UTC (Y-m-d'T'H:M:S'Z').par défaut, la clé peut être utilisée immédiatement."

  validation {
    error_message = "Veuillez fournir une date de début valide pour la clé au format UTC (Y-m-d'T'H:M:S'Z')."
    condition     = var.not_before_date == null ? true : can(regex("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z$", var.not_before_date))
  }
}
variable "tags" {
  type        = map(string)
  default     = {}
  description = "Un mapping de tags à assigner à la clé."
}
variable "rotation_policy" {
  type = object({
    automatic = optional(object({
      time_after_creation = optional(string, null)
      time_before_expiry  = optional(string, null)
    }), null)
    expire_after         = optional(string, null)
    notify_before_expiry = optional(string, null)
  })
  default     = null
  description = <<DESCRIPTION
La politique de rotation de la clé :

- `automatic` - La politique de rotation automatique de la clé.
  - `time_after_creation` - Le temps après la création de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.
  - `time_before_expiry` - Le temps avant l'expiration de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.
- `expire_after` - Le temps après lequel la clé expire.
- `notify_before_expiry` - Le temps avant l'expiration de la clé lorsque des emails de notification seront envoyés, exprimé en durée ISO 8601.
DESCRIPTION
}

variable "create_role_assignment_for_keys" {
  description = "Indique si une affectation de rôle doit être créée pour les clés."
  type        = bool
  default     = false
}

variable "role_assignments" {
  type = map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false)
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
    principal_type                         = optional(string, null)
  }))
  default     = {}
  description = <<DESCRIPTION
la map des affectations de rôles à créer sur la clé. La clé de la map est délibérément optionnel pour éviter les problèmes où les clés de la map peuvent être inconnues au moment du plan.

- `role_definition_id_or_name` - L'ID ou le nom de la définition de rôle à attribuer au principal. "Reader" et "Contributor" sont des exemples de noms de définition de rôle.
- `principal_id` - L'ID du principal à qui attribuer le rôle. Cela peut être l'ID d'un utilisateur, d'un groupe ou d'un service principal.
- `description` - La description de l'affectation de rôle.
- `skip_service_principal_aad_check` - Si défini sur true, ignore la vérification Azure Active Directory pour le service principal dans le locataire. Par défaut à false.
- `condition` - La condition qui sera utilisée pour délimiter l'affectation de rôle. La syntaxe de la condition est basée sur Azure Resource Manager. Pour plus d'informations, consultez https://learn.microsoft.com/en-us/azure/role-based-access-control/role-assignments.
- `condition_version` - La version de la syntaxe de condition. Si vous utilisez une condition, les valeurs valides sont '2.0'.

> Remarque : ne définissez `skip_service_principal_aad_check` sur vrai que si vous attribuez un rôle à un service principal.
DESCRIPTION
  nullable    = false
}
