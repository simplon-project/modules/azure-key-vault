<!-- BEGIN_TF_DOCS -->
 # Module: Keyvault/sub-modules/secret

Ce sous-module permet de crée les secrets dans un Azure Key Vault.
Il offre des fonctionnalités pour créer, récupérer, mettre à jour et supprimer des secrets dans un coffre-fort Key Vault. Les secrets peuvent être utilisés pour stocker des informations sensibles telles que des clés d'API, des chaînes de connexion de base de données, des certificats.

Utilisez ce module pour sécuriser vos secrets et garantir leur confidentialité dans votre application ou votre infrastructure Azure.

```hcl
resource "azurerm_key_vault_key" "this" {
  name            = var.name
  key_vault_id    = var.key_vault_resource_id
  key_type        = var.key_type
  key_size        = var.key_size
  key_opts        = var.key_opts
  curve           = var.curve
  expiration_date = timeadd(timestamp(), var.expiration_date)
  not_before_date = var.not_before_date
  tags            = var.tags
  dynamic "rotation_policy" {
    for_each = var.rotation_policy != null ? [var.rotation_policy] : []
    content {
      expire_after         = rotation_policy.value.expire_after
      notify_before_expiry = rotation_policy.value.notify_before_expiry

      automatic {
        time_before_expiry = rotation_policy.value.automatic.time_before_expiry
      }
    }
  }
}

resource "azurerm_role_assignment" "this" {
  for_each = var.create_role_assignment ? var.role_assignments : {}

  principal_id                           = each.value.role_assignment.principal_id
  scope                                  = azurerm_key_vault_key.this
  condition                              = each.value.role_assignment.condition
  condition_version                      = each.value.role_assignment.condition_version
  delegated_managed_identity_resource_id = each.value.role_assignment.delegated_managed_identity_resource_id
  principal_type                         = each.value.principal_type
  role_definition_id                     = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? each.value.role_assignment.role_definition_id_or_name : null
  role_definition_name                   = strcontains(lower(each.value.role_assignment.role_definition_id_or_name), lower(local.role_definition_resource_substring)) ? null : each.value.role_assignment.role_definition_id_or_name
  skip_service_principal_aad_check       = each.value.role_assignment.skip_service_principal_aad_check
}
```

<!-- markdownlint-disable MD033 -->
## Requirements

The following requirements are needed by this module:

- <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) (>= 1.0.0)

- <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) (>= 3.110.0)

## Providers

The following providers are used by this module:

- <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) (>= 3.110.0)

## Resources

The following resources are used by this module:

- [azurerm_key_vault_key.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_key) (resource)
- [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/role_assignment) (resource)

<!-- markdownlint-disable MD013 -->
## Required Inputs

The following input variables are required:

### <a name="input_key_vault_resource_id"></a> [key\_vault\_resource\_id](#input\_key\_vault\_resource\_id)

Description: The ID of the Key Vault where the key should be created.

Type: `string`

### <a name="input_name"></a> [name](#input\_name)

Description: Nom de la clé.

Type: `string`

## Optional Inputs

The following input variables are optional (have default values):

### <a name="input_create_role_assignment"></a> [create\_role\_assignment](#input\_create\_role\_assignment)

Description: Indique si une affectation de rôle doit être créée

Type: `bool`

Default: `false`

### <a name="input_curve"></a> [curve](#input\_curve)

Description:  la courbe elliptique de la clé. Utilisé pour les clés de type EC et EC-HSM. Les valeurs valides sont P-256, P-384, P-521, P-256K, P-384K, P-521K.

Type: `string`

Default: `"P-256"`

### <a name="input_expiration_date"></a> [expiration\_date](#input\_expiration\_date)

Description: La date d'expiration de la clé au format UTC (Y-m-d'T'H:M:S'Z'),par défaut, la clé expire dans un an.

Type: `string`

Default: `"8760"`

### <a name="input_key_opts"></a> [key\_opts](#input\_key\_opts)

Description: Les options de la clé.

Type: `list(string)`

Default:

```json
[
  "decrypt",
  "encrypt",
  "sign",
  "unwrapKey",
  "verify",
  "wrapKey"
]
```

### <a name="input_key_size"></a> [key\_size](#input\_key\_size)

Description: La taille de la clé.

Type: `number`

Default: `2048`

### <a name="input_key_type"></a> [key\_type](#input\_key\_type)

Description: Le type de la clé. RSA, EC, EC-HSM, RSA-HSM3

Type: `string`

Default: `"RSA"`

### <a name="input_not_before_date"></a> [not\_before\_date](#input\_not\_before\_date)

Description: La date à partir de laquelle la clé peut être utilisée au format UTC (Y-m-d'T'H:M:S'Z').par défaut, la clé peut être utilisée immédiatement.

Type: `string`

Default: `null`

### <a name="input_role_assignments"></a> [role\_assignments](#input\_role\_assignments)

Description: la map des affectations de rôles à créer sur la clé. La clé de la map est délibérément optionnel pour éviter les problèmes où les clés de la map peuvent être inconnues au moment du plan.

- `role_definition_id_or_name` - L'ID ou le nom de la définition de rôle à attribuer au principal. "Reader" et "Contributor" sont des exemples de noms de définition de rôle.
- `principal_id` - L'ID du principal à qui attribuer le rôle. Cela peut être l'ID d'un utilisateur, d'un groupe ou d'un service principal.
- `description` - La description de l'affectation de rôle.
- `skip_service_principal_aad_check` - Si défini sur true, ignore la vérification Azure Active Directory pour le service principal dans le locataire. Par défaut à false.
- `condition` - La condition qui sera utilisée pour délimiter l'affectation de rôle. La syntaxe de la condition est basée sur Azure Resource Manager. Pour plus d'informations, consultez https://learn.microsoft.com/en-us/azure/role-based-access-control/role-assignments.
- `condition_version` - La version de la syntaxe de condition. Si vous utilisez une condition, les valeurs valides sont '2.0'.

> Remarque : ne définissez `skip_service_principal_aad_check` sur vrai que si vous attribuez un rôle à un service principal.

Type:

```hcl
map(object({
    role_definition_id_or_name             = string
    principal_id                           = string
    description                            = optional(string, null)
    skip_service_principal_aad_check       = optional(bool, false)
    condition                              = optional(string, null)
    condition_version                      = optional(string, null)
    delegated_managed_identity_resource_id = optional(string, null)
    principal_type                         = optional(string, null)
  }))
```

Default: `{}`

### <a name="input_rotation_policy"></a> [rotation\_policy](#input\_rotation\_policy)

Description: La politique de rotation de la clé :

- `automatic` - La politique de rotation automatique de la clé.
  - `time_after_creation` - Le temps après la création de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.
  - `time_before_expiry` - Le temps avant l'expiration de la clé avant qu'elle ne soit automatiquement tournée, exprimé en durée ISO 8601.
- `expire_after` - Le temps après lequel la clé expire.
- `notify_before_expiry` - Le temps avant l'expiration de la clé lorsque des emails de notification seront envoyés, exprimé en durée ISO 8601.

Type:

```hcl
object({
    automatic = optional(object({
      time_after_creation = optional(string, null)
      time_before_expiry  = optional(string, null)
    }), null)
    expire_after         = optional(string, null)
    notify_before_expiry = optional(string, null)
  })
```

Default: `null`

### <a name="input_tags"></a> [tags](#input\_tags)

Description: Un mapping de tags à assigner à la clé.

Type: `map(string)`

Default: `{}`

## Outputs

The following outputs are exported:

### <a name="output_key_id"></a> [key\_id](#output\_key\_id)

Description: La clé générée par le Key Vault

### <a name="output_resource_id"></a> [resource\_id](#output\_resource\_id)

Description: l'id du secret généré par le Key Vault

### <a name="output_resource_versionless_id"></a> [resource\_versionless\_id](#output\_resource\_versionless\_id)

Description: L'ID de ressource Azure sans version du secret.

### <a name="output_versionless_id"></a> [versionless\_id](#output\_versionless\_id)

Description: L'ID de base de la clé du Key Vault

## Modules

No modules.

## Documentation

[**keyvault\_secret**](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/key_vault_secret)

## Auteur

[**Kingston-run**](https://gitlab.com/Kingston-run)
<!-- END_TF_DOCS -->
